module Main exposing(main)

import Html exposing(..)
import Html.Attributes exposing(..)

--PÁGINAS DE BASE
--http://elm-bootstrap.info/navbar
--https://package.elm-lang.org/packages/rundis/elm-bootstrap/5.2.0/Bootstrap-Modal
--https://elmprogramming.com/model-view-update-part-2.html
--LIBRO(Pág 71) file:///C:/Users/raunel%20perez/Downloads/programming-elm-maintainable-front-end-applications.pdf

main : Html msg
main =
    div [][
        div[class "header"][
            h1[][text "Listado de candidatos"]
        ]
        ,div [class "container"][
            button[class "btn btn-success"][text "Agregar candidatos"]
        ]
        ,br[][]
        ,div [class "container"][
            table [ class "table table-striped"  ]
                [ tr []
                    [ th [] [ text "#" ]
                    , th [] [ text "Nombre" ]
                    , th [] [ text "Vacante" ]
                    , th [] [ text "Correo" ]
                    , th [] [ text "Clave" ]
                    , th [] [ text "Fecha" ]
                    , th [] [ text "Avance" ]
                    ]
                , tr []
                    [ td [] [ text "1" ]
                    , td [] [ text "Raunel" ]
                    , td [] [ text "Desarrollador" ]
                    , td [] [ text "raunel_95@live.com.mx" ]
                    , td [] [ text "1312@d3skjs" ]
                    , td [] [ text "09/04/2020" ]
                    , td [] [ button[class "btn btn-success"][text "80%"] ]
                    ]
                , tr []
                    [ td [] [ text "2" ]
                    , td [] [ text "Ulises" ]
                    , td [] [ text "Desarrollador" ]
                    , td [] [ text "Ulises_9x@live.com.mx" ]
                    , td [] [ text "1312@d3skjs" ]
                    , td [] [ text "09/04/2020" ]
                    , td [] [ button[class "btn btn-success"][text "80%"] ]
                    ]
                ]
        ]
    ]